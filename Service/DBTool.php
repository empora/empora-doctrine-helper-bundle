<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Empora\Doctrine\HelperBundle\Service;

use Doctrine\Common\Util\ClassUtils;
use Empora\Doctrine\HelperBundle\ORM\Entity\Interfaces\DBEntity as DBEntityInterface;
use Empora\Doctrine\HelperBundle\ORM\Repository\BaseRepository;

/**
 * Description of Tool
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class DBTool {

	const ASC = 'ASC';
	const DESC = 'DESC';
	const MIN_FLUSH_ITERATOR_COUNT = 30;

	/**
	 * @param \Traversable|array $haystack
	 * @param BaseRepository     $repository
	 * @param callable           $callback function($element, $repo) {}
	 * @param int                $flushIteratorCount
	 *
	 * @throws \InvalidArgumentException
	 */
	public static function flushIterator($haystack, BaseRepository $repository, $callback, $flushIteratorCount = 100) {
		$flushIteratorCount = (int)$flushIteratorCount;
		if (!(is_array($haystack) || $haystack instanceof \Traversable)) {
			throw new \InvalidArgumentException('Haystack must be an array or instance of \Traversable');
		}
		if (!is_callable($callback)) {
			throw new \InvalidArgumentException('callback ist not callable');
		}
		if ($flushIteratorCount < self::MIN_FLUSH_ITERATOR_COUNT) {
			throw new \InvalidArgumentException(sprintf('FlushIteratorCount have to greater or equal %u', self::MIN_FLUSH_ITERATOR_COUNT));
		}
		$i = 0;
		foreach ($haystack AS $entity) {
			$callback($entity, $repository);
			$i++;
			if (!($i % $flushIteratorCount)) {
				$repository->flush();
			}
		}
		$repository->flush();
	}

	/**
	 * @param        $sort
	 * @param string $default
	 *
	 * @return string
	 */
	public static function orderByValidate($sort, $default = self::ASC) {
		$sort = strtoupper($sort);
		if (!in_array($sort, array(self::ASC, self::DESC))) {
			$sort = self::orderByValidate($default, self::ASC);
		}
		return $sort;
	}

	/**
	 * @param string $field
	 * @param string $entityName
	 *
	 * @return string
	 */
	public static function entityField($field, $entityName = BaseRepository::ENTITY) {
		return sprintf('%s.%s', $entityName, $field);
	}

	/**
	 * @param string $className
	 *
	 * @throws \InvalidArgumentException
	 * @return string
	 */
	public static function discriminatorByClass($className) {
		if (!is_string($className)) {
			throw new \InvalidArgumentException('Argument "classname" should be a string, given: ' . gettype($className));
		}
		if (strpos($className, "\\") === false) { //should never be!
			return strtolower($className);
		}

		$parts = explode("\\", $className);
		return strtolower(end($parts));
	}

	/**
	 * @param DBEntityInterface $entity
	 *
	 * @return string
	 */
	public static function discriminatorByEntity(DBEntityInterface $entity) {
		return self::discriminatorByClass(ClassUtils::getClass($entity));
	}
}
