<?php

namespace Empora\Doctrine\HelperBundle\Service;

/**
 * Description of Logger
 * distribute the doctrine logger
 * 
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class Logger {

	/**
	 * @var \Doctrine\Bundle\DoctrineBundle\Registry
	 */
	private $doctrine;

	/**
	 * @param \Doctrine\Bundle\DoctrineBundle\Registry $doctrine
	 */
	function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine) {
		$this->doctrine = $doctrine;
	}

	/**
	 * CacheLoggerChain
	 * @return \Doctrine\ORM\Cache\Logging\StatisticsCacheLogger
	 */
	public function getStatisticsCacheLogger() {
		return null; //disable it
		static $statisticsCacheLogger;
		if (!($statisticsCacheLogger instanceof \Doctrine\ORM\Cache\Logging\StatisticsCacheLogger)) {
			$chainLogger = $this->doctrine->getManager()->getConfiguration()->getSecondLevelCacheConfiguration()->getCacheLogger();
			if ($chainLogger instanceof \Doctrine\ORM\Cache\Logging\CacheLoggerChain) {
				/* @var $chainLogger \Doctrine\ORM\Cache\Logging\CacheLoggerChain */
				foreach ($chainLogger->getLoggers() AS $logger) {
					if ($logger instanceof \Doctrine\ORM\Cache\Logging\StatisticsCacheLogger) {
						$statisticsCacheLogger = $logger;
						break;
					}
				}
			}
		}
		return $statisticsCacheLogger;
	}

}
