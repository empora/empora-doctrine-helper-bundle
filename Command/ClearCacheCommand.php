<?php

namespace Empora\Doctrine\HelperBundle\Command;

use Doctrine\Bundle\DoctrineBundle\Command\Proxy\DoctrineCommandHelper;
use Doctrine\Common\Cache\CacheProvider;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of ClearCacheCommand
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class ClearCacheCommand extends ContainerAwareCommand {

    const COMMAND_NAME = 'empora:doctrine:helper:clearcache';
    const ARGUMENT_CACHE = 'cache';
    const OPTION_FLUSH = 'flush';
    const OPTION_ENTITY_MANAGER = 'em';

    /**
     * @var LoggerInterface
     */
    private $logger;

    protected function configure() {
        $this->setName(self::COMMAND_NAME);
        $this->addArgument(self::ARGUMENT_CACHE, InputArgument::REQUIRED, 'Use METADATA|QUERY|RESULT');
        $this->addOption(self::OPTION_FLUSH, 'f', InputOption::VALUE_NONE);
        $this->addOption(self::OPTION_ENTITY_MANAGER, null, InputOption::VALUE_OPTIONAL, 'The entity manager to use for this command');
    }

    public function initialize(InputInterface $input, OutputInterface $output) {
        $this->logger = $this->getContainer()->get('logger');
        DoctrineCommandHelper::setApplicationEntityManager($this->getApplication(), $input->getOption('em'));
    }

    public function execute(InputInterface $input, OutputInterface $output) {
        $em = $this->getHelper('em')->getEntityManager();
        $service = $this->getContainer()->get('Empora.Doctrine.ClearCache');
        /* @var $service \Empora\Doctrine\HelperBundle\Service\ClearCache */
        $service->clearCache($input->getArgument(self::ARGUMENT_CACHE), $em, $input->getOption(self::OPTION_FLUSH));
    }

}
