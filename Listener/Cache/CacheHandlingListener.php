<?php

namespace Empora\Doctrine\HelperBundle\Listener\Cache;

use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\Cache;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Empora\Doctrine\HelperBundle\ORM\Entity\Interfaces\DBEntity;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Description of CacheHandlingListener
 * 
 * - Remove Entity from cache after "Delete"
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 * DI\DoctrineListener(
 *     events = {"preRemove"},
 *     lazy = true
 * )
 */
class CacheHandlingListener {

	/**
	 * Delete all removed entities from cache
	 * @param LifecycleEventArgs $args
	 */
	public function preRemove(LifecycleEventArgs $args) {
		$slc = $args->getEntityManager()->getCache();
		$entity = $args->getEntity();
		if ($slc instanceof Cache && $entity instanceof DBEntity) {
			$slc->evictEntity(ClassUtils::getClass($entity), $entity->getId());
		}
	}

}
