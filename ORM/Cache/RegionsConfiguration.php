<?php

namespace Empora\Doctrine\HelperBundle\ORM\Cache;

use Empora\Doctrine\HelperBundle\Metadata\RegionLifetime\Factory\QueryRegionLifetimeFactory;
use Empora\Doctrine\HelperBundle\Metadata\RegionLifetime\RegionLifetimeResolver;


/**
 * Description of RegionsConfiguration
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class RegionsConfiguration extends \Doctrine\ORM\Cache\RegionsConfiguration {

	/**
	 * @var RegionLifetimeResolver
	 */
	protected $regionLifetimeResolver;

	public function getLifetime($regionName) {
		$lifetime = $this->regionLifetimeResolver->resolveRegionToLifetime($regionName);
		if (!is_null($lifetime)) {
			$this->setLifetime($regionName, $lifetime);
		}
		return parent::getLifetime($regionName);
	}

	/**
	 * @param \Empora\Doctrine\HelperBundle\Metadata\RegionLifetime\RegionLifetimeResolver $regionLifetimeResolver
	 */
	public function setRegionLifetimeResolver($regionLifetimeResolver) {
		$this->regionLifetimeResolver = $regionLifetimeResolver;
	}

	/**
	 * @return \Empora\Doctrine\HelperBundle\Metadata\RegionLifetime\RegionLifetimeResolver
	 */
	public function getRegionLifetimeResolver() {
		return $this->regionLifetimeResolver;
	}

}