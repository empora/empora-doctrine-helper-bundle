<?php

namespace Empora\Doctrine\HelperBundle\ORM\Cache;

use Doctrine\ORM\Cache;
use Doctrine\ORM\EntityManagerInterface;
use Empora\Doctrine\HelperBundle\Metadata\RegionLifetime\Factory\QueryRegionLifetimeFactory;


/**
 * Description of SecondLevelCacheFactory
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class DefaultCacheFactory extends \Doctrine\ORM\Cache\DefaultCacheFactory {

	/**
	 * Empora.Doctrine.Helper.Metadata.Factory.QueryRegionLifetime
	 *
	 * @var QueryRegionLifetimeFactory
	 */
	private $queryRegionLifetimeMetadataFactory;

	/**
	 * @param EntityManagerInterface $em
	 * @param string                 $regionName
	 *
	 * @return Cache\QueryCache
	 */
	public function buildQueryCache(EntityManagerInterface $em, $regionName = null) {
		$queryCache = parent::buildQueryCache($em, $regionName);
		return $queryCache;
	}
}