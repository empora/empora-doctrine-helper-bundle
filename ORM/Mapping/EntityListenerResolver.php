<?php

namespace Empora\Doctrine\HelperBundle\ORM\Mapping;

use Doctrine\ORM\Mapping\DefaultEntityListenerResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of EntityListenerResolver
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class EntityListenerResolver extends DefaultEntityListenerResolver {
	
	const SERVICE_NAME = 'Empora.Doctrine.Helper.Service.Resolver';

	private $container;
	private $mapping = array();

	/**
	 * 
	 * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
	 */
	public function __construct(ContainerInterface $container) {
		$this->container = $container;
	}

	public function addMapping($className, $service) {
		$this->mapping[$className] = $service;
	}

	public function resolve($className) {
		if (isset($this->mapping[$className]) && $this->container->has($this->mapping[$className])) {
			return $this->container->get($this->mapping[$className]);
		}
		return parent::resolve($className);
	}

}
