<?php

namespace Empora\Doctrine\HelperBundle\ORM\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of Dated
 * 
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
trait Dated {

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt = null;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt = null;

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     */
    public function _prePersist() {
        $this->updatedAt = $this->createdAt = new \DateTime();
    }

    /**
     * Set updatedAt
     *
     * @ORM\PreUpdate
     */
    public function _preUpdate() {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

} 