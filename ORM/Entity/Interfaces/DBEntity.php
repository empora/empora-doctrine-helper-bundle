<?php

namespace Empora\Doctrine\HelperBundle\ORM\Entity\Interfaces;

/**
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
interface DBEntity {
    
    function getId();
    
}

?>
