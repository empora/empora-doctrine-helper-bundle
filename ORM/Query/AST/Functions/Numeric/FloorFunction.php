<?php

namespace Empora\Doctrine\HelperBundle\ORM\Query\AST\Functions\Numeric;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Description of IfFunction
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class FloorFunction extends FunctionNode {

	private $simpleArithmeticExpression;

	public function parse(Parser $parser) {
		$parser->match(Lexer::T_IDENTIFIER);
		$parser->match(Lexer::T_OPEN_PARENTHESIS);
		$this->simpleArithmeticExpression = $parser->SimpleArithmeticExpression();
		$parser->match(Lexer::T_CLOSE_PARENTHESIS);
	}

	public function getSql(SqlWalker $sqlWalker) {
		return sprintf(
			'FLOOR(%s)',
			$sqlWalker->walkSimpleArithmeticExpression(
				$this->simpleArithmeticExpression
			)
		);
	}
}