<?php

namespace Empora\Doctrine\HelperBundle\ORM\Query\AST\Functions\String;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Description of IfFunction
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class IfFunction extends FunctionNode {
	private $expr = array();

	public function parse(Parser $parser) {
		$parser->match(Lexer::T_IDENTIFIER);
		$parser->match(Lexer::T_OPEN_PARENTHESIS);
		$this->expr[] = $parser->ConditionalExpression();

		for ($i = 0; $i < 2; $i++) {
			$parser->match(Lexer::T_COMMA);
			$this->expr[] = $parser->ArithmeticExpression();
		}

		$parser->match(Lexer::T_CLOSE_PARENTHESIS);
	}

	public function getSql(SqlWalker $sqlWalker) {
		return sprintf('IF(%s, %s, %s)',
			$sqlWalker->walkConditionalExpression($this->expr[0]),
			$sqlWalker->walkArithmeticPrimary($this->expr[1]),
			$sqlWalker->walkArithmeticPrimary($this->expr[2]));
	}
}