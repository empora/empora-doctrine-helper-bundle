<?php

namespace Empora\Doctrine\HelperBundle\ORM\Query\AST\Functions\Datetime;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;


/**
 * Description of YearFunction
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class YearFunction extends FunctionNode {

	/**
	 * @var string
	 */
	protected $date;

	public function parse(Parser $parser) {
		$lexer = $parser->getLexer();
		$parser->match(Lexer::T_IDENTIFIER);
		$parser->match(Lexer::T_OPEN_PARENTHESIS);
		$this->date = $parser->ArithmeticPrimary();
		$parser->match(Lexer::T_CLOSE_PARENTHESIS);
	}

	public function getSql(SqlWalker $sqlWalker) {
		return sprintf(
			'YEAR(%s)',
			$sqlWalker->walkArithmeticPrimary($this->date)
		);
	}
}
