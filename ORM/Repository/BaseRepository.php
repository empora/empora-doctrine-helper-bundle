<?php

namespace Empora\Doctrine\HelperBundle\ORM\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Empora\Doctrine\HelperBundle\ORM\Entity\Interfaces\DBEntity AS DBEntityInterface;
use Empora\Doctrine\HelperBundle\ORM\Exception\EntityException;
use Empora\Doctrine\HelperBundle\Service\DBTool;

class BaseRepository extends EntityRepository {

    const ENTITY = 'entity';

    private $debugMode = false;

    public function getDebugMode() {
        return (bool)$this->debugMode;
    }

    public function setDebugMode($debugMode) {
        $this->debugMode = (bool)$debugMode;
    }

    /**
     * @return QueryBuilder
     */
    public function getQuery() {
        return $this->getDefaultQueryBuilder()->getQuery();
    }

    /**
     * @param string|null $indexBy
     *
     * @return QueryBuilder
     */
    public function getDefaultQueryBuilder($indexBy = null) {
        return $this->createQueryBuilder(self::ENTITY, $indexBy);
    }

    /**
     * @param DBEntityInterface $entity
     * @throws EntityException
     */
    public function persist(DBEntityInterface $entity) {
        if ($this->debugMode && $entity->getId()) {
            if (!$this->getEntityManager()->contains($entity)) {
                throw EntityException::rePersist($entity);
            }
        }
        $this->getEntityManager()->persist($entity);
    }

    /**
     * @param DBEntityInterface $entity
     */
    public function remove(DBEntityInterface $entity) {
        $this->getEntityManager()->remove($entity);
    }

    /**
     * @param DBEntityInterface $entity
     */
    public function flush(DBEntityInterface $entity = null) {
        $this->getEntityManager()->flush($entity);
    }

    /**
     * @param DBEntityInterface $entity
     * @return DBEntityInterface
     */
    public function save(DBEntityInterface $entity) {
        $this->persist($entity);
        $this->flush($entity);
        return $entity;
    }

    /**
     * @param DBEntityInterface $entity
     */
    public function delete(DBEntityInterface $entity) {
        $this->remove($entity);
        $this->flush($entity);
    }

    /**
     * @param DBEntityInterface $entity
     */
    public function detach(DBEntityInterface $entity) {
        $this->getEntityManager()->detach($entity);
    }

    /**
     * Get Entity by "id"
     * Alias for "find"
     * @param int $id
     * @return DBEntityInterface
     * @see find
     */
    public function getById($id) {
        return $this->find((int)$id);
    }

    /**
     * @param int[] $ids
     * @return DBEntityInterface[]
     */
    public function getByIds(array $ids) {
        $qb = $this->getDefaultQueryBuilder();
        return $qb
            ->where(
                $qb->expr()->in(DBTool::entityField('id'), ':ids')
            )
            ->setParameter('ids', array_map('intval', $ids))
            ->getQuery()->execute();
    }

    /**
     * @param int $amount
     * @param int $offset
     * @return DBEntityInterface[]
     */
    public function getAll($amount = null, $offset = null) {
        return $this->getAllQueryBuilder($amount, $offset)->getQuery()->execute();
    }

    /**
     * @param int $amount
     * @param int $offset
     * @return QueryBuilder
     */
    protected function getAllQueryBuilder($amount = null, $offset = null) {
        return $this->getDefaultQueryBuilder()->setMaxResults($amount)->setFirstResult($offset);
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager() {
        return parent::getEntityManager();
    }

    /**
     * Truncate Table -- disable the FOREIGN_KEY_CHECKS
     * @return boolean
     */
    public function truncate() {
        $classMetadata = $this->getClassMetadata();
        $connection = $this->getEntityManager()->getConnection();
        $connection->beginTransaction();
        try {
            $q = $connection->getDatabasePlatform()->getTruncateTableSql(
                $classMetadata->getTableName()
            );
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $connection->executeUpdate($q);
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
            $connection->commit();
            return true;
        } catch (\Exception $e) {
            $connection->rollback();
            return false;
        }
    }

    /**
     * Delete ALL entitys from table -- disable the FOREIGN_KEY_CHECKS
     * @return boolean
     */
    public function deleteAll() {
        $classMetadata = $this->getClassMetadata();
        $connection = $this->getEntityManager()->getConnection();
        $connection->beginTransaction();
        try {
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $connection->query('DELETE FROM ' . $classMetadata->getTableName());
            // Beware of ALTER TABLE here -- it's another DDL statement
            // and will cause an implicit commit.
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
            $connection->commit();
            return true;
        } catch (\Exception $e) {
            $connection->rollback();
            return false;
        }
    }

    /**
     * Set the Auto-increment value
     * @param integer $index
     * @return boolean
     */
    public function setAutoIncrement($index) {
        $classMetadata = $this->getClassMetadata();
        $connection = $this->getEntityManager()->getConnection();
        $connection->beginTransaction();
        try {
            $params = [
                'index' => $index
            ];
            $types = [
                'index' => Type::INTEGER
            ];
            $connection->executeQuery(
                "ALTER TABLE {$classMetadata->getTableName()} AUTO_INCREMENT = :index", $params, $types
            );
            $connection->commit();
            return true;
        } catch (\Exception $e) {
            $connection->rollback();
            return false;
        }
    }

    /**
     * page through given query, prevent to use a custom order style by default it will add order by "id ASC"
     * @param QueryBuilder $query
     * @param \Closure     $closure
     * @param int          $pageSize
     * @param bool         $clearEntityManager
     */
    protected function pageThrough(QueryBuilder $query, \Closure $closure, $pageSize = 100, $clearEntityManager = true) {
        $nextPage = true;
        $id = 0;
        $query->setMaxResults($query->getMaxResults() ?: $pageSize);
        $query->andWhere($query->expr()->gt(DBTool::entityField('id'), ':id'));
        $query->addOrderBy(DBTool::entityField('id'), DBTool::ASC);
        while (
            $nextPage &&
            ($entities = $query->setParameter('id', $id, Type::INTEGER)->getQuery()->execute())
        ) {
            if ($nextPage = (bool)$closure($entities)) {
                $lastEntity = end($entities);
                if ($nextPage = ($lastEntity instanceof DBEntityInterface)) {
                    /** @var DBEntityInterface $lastEntity */
                    $id = $lastEntity->getId();
                }
            }
            if ($clearEntityManager) {
                $this->clear(); //clean EntityManager, will cleanup the runtime cache
            }
        }
    }

}
