<?php

namespace Empora\Doctrine\HelperBundle\Logger;

use Doctrine\ORM\Cache\CollectionCacheKey;
use Doctrine\ORM\Cache\EntityCacheKey;
use Doctrine\ORM\Cache\Logging\CacheLogger AS CacheLoggerInterface;
use Doctrine\ORM\Cache\QueryCacheKey;
use Psr\Log\LoggerInterface;

/**
 * Description of CacheLogger
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class CacheLogger implements CacheLoggerInterface {

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @param LoggerInterface $logger
	 */
	function __construct(LoggerInterface $logger) {
		$this->logger = $logger;
	}

	public function collectionCacheHit($regionName, CollectionCacheKey $key) {
		$this->logger->debug(sprintf('CollectionCacheHit: %s', $regionName), array($key));
	}

	public function collectionCacheMiss($regionName, CollectionCacheKey $key) {
		$this->logger->debug(sprintf('CollectionCacheMiss: %s', $regionName), array($key));
	}

	public function collectionCachePut($regionName, CollectionCacheKey $key) {
		$this->logger->debug(sprintf('CollectionCachePut: %s', $regionName), array($key));
	}

	public function entityCacheHit($regionName, EntityCacheKey $key) {
		$this->logger->debug(sprintf('EntityCacheHit: %s', $regionName), array($key));
	}

	public function entityCacheMiss($regionName, EntityCacheKey $key) {
		$this->logger->debug(sprintf('EntityCacheMiss: %s', $regionName), array($key));
	}

	public function entityCachePut($regionName, EntityCacheKey $key) {
		$this->logger->debug(sprintf('EntityCachePut: %s', $regionName), array($key));
	}

	public function queryCacheHit($regionName, QueryCacheKey $key) {
		$this->logger->debug(sprintf('QueryCacheHit: %s', $regionName), array($key));
	}

	public function queryCacheMiss($regionName, QueryCacheKey $key) {
		$this->logger->debug(sprintf('QueryCacheMiss: %s', $regionName), array($key));
	}

	public function queryCachePut($regionName, QueryCacheKey $key) {
		$this->logger->debug(sprintf('QueryCachePut: %s', $regionName), array($key));
	}

}
