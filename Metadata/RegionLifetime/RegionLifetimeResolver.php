<?php

namespace Empora\Doctrine\HelperBundle\Metadata\RegionLifetime;

use Doctrine\Common\Collections\ArrayCollection;
use Empora\Doctrine\HelperBundle\Annotation\Cache\QueryRegionLifetime;
use Empora\Doctrine\HelperBundle\Annotation\Cache\RegionLifetimeInterface;

/**
 * Description of RegionLifetimeResolver
 *
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 * @todo runtimeCache to xcache (doctrine cache ?)
 * @todo better handling for diffrent lifetime metadata implementations -> eg. QueryRegionLifetime
 */
class RegionLifetimeResolver {

    private $runtimeCache = [];

    /**
     * @var ArrayCollection
     */
    private $regionLifetimeList;

    public function __construct() {
        $this->regionLifetimeList = new ArrayCollection();
    }

    /**
     * @param RegionLifetimeInterface $regionLifetime
     */
    public function addRegionLifetime(RegionLifetimeInterface $regionLifetime) {
        $this->regionLifetimeList->add($regionLifetime);
    }

    /**
     * @param array|\Traversable $regionLifetimes array of RegionLifetimeInterface
     */
    public function addRegionLifetimes($regionLifetimes) {
        if (is_array($regionLifetimes) || $regionLifetimes instanceof \Traversable) {
            foreach ($regionLifetimes AS $regionLifetime) {
                $this->addRegionLifetime($regionLifetime);
            }
        }
    }

    /**
     * @param $region
     *
     * @return int|null
     */
    public function resolveRegionToLifetime($region) {
        if (isset($this->runtimeCache[$region])) {
            return $this->runtimeCache[$region];
        } else {
            $region = (string)$region;
            foreach ($this->regionLifetimeList AS $regionLifetime) {
                /* @var $regionLifetime RegionLifetimeInterface */
                if ($regionLifetime instanceof QueryRegionLifetime) {
                    /* @var $regionLifetime QueryRegionLifetime */
                    $lifetime = $this->handleQueryRegionLifetime($region, $regionLifetime);
                    if (!is_null($lifetime)) {
                        return $this->runtimeCache[$region] = $lifetime;
                    }
                }
            }
        }
        return null;
    }

    /**
     * @param string              $region
     * @param QueryRegionLifetime $regionLifetime
     *
     * @return int|null
     */
    protected function handleQueryRegionLifetime($region, QueryRegionLifetime $regionLifetime) {
        if (
            $this->stringBeginsWith($region, $regionLifetime->getPrefix()) ||
            $this->stringEndsWith($region, $regionLifetime->getSuffix()) ||
            $this->stringBeginsWith($region, $regionLifetime->getAffix()) ||
            $this->stringEndsWith($region, $regionLifetime->getAffix())
        ) {
            return $regionLifetime->getLifetime();
        }
        return null;
    }

    protected function stringBeginsWith($str, $sub, $caseSensitive = false) {
        return strlen($str) && strlen($sub) && (substr_compare($str, $sub, 0, strlen($sub), !$caseSensitive) === 0);
    }

    /**
     * @param string $str
     * @param string $sub
     * @param bool   $caseSensitive
     *
     * @return boolean
     */
    protected function stringEndsWith($str, $sub, $caseSensitive = false) {
        return (string)$str && (string)$sub && substr_compare($str, $sub, -strlen($sub), strlen($sub), !$caseSensitive) === 0;
    }
}