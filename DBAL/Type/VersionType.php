<?php

namespace Empora\Doctrine\HelperBundle\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Empora\Doctrine\HelperBundle\Common\Version\Version;

/**
 * convert version to int and revert
 */
class VersionType extends Type {

	const __TYPE_NAME = 'version';

	public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform) {
		return "INT(10) COMMENT '(DC2Type:" . static::__TYPE_NAME . ")'";
	}

	public function convertToPHPValue($value, AbstractPlatform $platform) {
		if (!is_null($value)) {
			return Version::parse(long2ip($value), Version::DEFAULT_FORMAT);
		}
		return null;
	}

	public function convertToDatabaseValue($value, AbstractPlatform $platform) {
		if ($value instanceof Version) {
			return ip2long($value->format(Version::DEFAULT_FORMAT));
		}
		return null;
	}

	public function getBindingType() {
		return \PDO::PARAM_INT;
	}

	public function getName() {
		return static::__TYPE_NAME;
	}
}
