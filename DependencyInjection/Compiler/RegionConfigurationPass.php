<?php

namespace Empora\Doctrine\HelperBundle\DependencyInjection\Compiler;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;


/**
 * Description of RegionConfigurationPass
 * 
 * @author Volker von Hoesslin <volker.von.hoesslin@empora.com>
 */
class RegionConfigurationPass implements CompilerPassInterface {

	/**
	 * You can modify the container here before it is dumped to PHP code.
	 *
	 * @param ContainerBuilder $container
	 *
	 * @api
	 */
	public function process(ContainerBuilder $container) {
		$entityManagerName = 'default'; //$entityManager['name']
		$regionConfDef = $container->getDefinition(sprintf('doctrine.orm.%s_second_level_cache.regions_configuration', $entityManagerName));
		$regionConfDef->addMethodCall('setRegionLifetimeResolver', array(new Reference('Empora.Doctrine.Helper.Metadata.RegionLifetime.Resolver')));
	}
}